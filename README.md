# pamac-flatpak-mobile
[pamac-all](https://aur.archlinux.org/packages/pamac-all/) (snap disabled) PKGBUILD modified for use on mobile. [Pamac](https://gitlab.manjaro.org/applications/pamac) is a graphical package manager for Manjaro Linux with Alpm, AUR, Appstream, Flatpak and Snap support. This particular distribution of it is derived from [pamac-all](https://aur.archlinux.org/packages/pamac-all/) on the AUR. It disables Snap (see the [enabling snap](#enabling-snap) section if you wish to enable it) and changes the gtk3 dependency to gtk3-mobile. This is for a faster and easier install on platforms such as [Pine64-Arch](https://github.com/dreemurrs-embedded/Pine64-Arch) running [Phosh](https://source.puri.sm/Librem5/phosh/).

## Obtaining
The only necessary file is the pkg.tar.xz file. This is a built package that you can simply install. You can obtain it from the [.packages](.packages) directory or from the [releases](https://gitlab.com/nickgirga/pamac-flatpak-mobile/-/releases) page. If you wish to rebuild this package yourself, you can copy this repository to your device by running `git clone https://gitlab.com/nickgirga/pamac-flatpak-mobile.git` and head to the [building](#building) section.

## Enabling Flatpak
Flatpak is built with the package by default, so all you have to do is enable it in the preferences after [installation](#installation). [Launch Pamac](#running) and press the button with the 3 dots in the top right corner. Then, press "Preferences" and enter your user password. Go to the "Flatpak" tab and flip "Enable Flatpak Support" on. Optionally, you can check the "Check for updates" checkbox if you want your Flatpak software to stay up-to-date when updating using Pamac.

## Enabling Snap
For personal reasons, I have chosen to leave Snap disabled by default. To enable it, you must rebuild the package using the PKGBUILD. The easiest way to get started is to clone the repository as mentioned in the [obtaining](#obtaining) section. Then, edit the PKGBUILD file with the text editor of your choice and change `ENABLE_SNAPD=0` on line 6 to `ENABLE_SNAPD=1`. Once you have saved your changes to the PKGBUILD file, head to the [building](#building) section.

## Building
This step is only necessary if you choose not to use the built pkg.tar.xz package file. Otherwise, skip to the [installation](#installation) section. To build a package using the PKGBUILD, simply run `makepkg -s` in the same directory as the PKGBUILD file. If there is already a built package of the same version in the same directory as your PKGBUILD, run `makepkg -f` to force it to build over it. Once you have a prepared package file, head to the [installation](#installation) section.

## Installation
If built using the PKGBUILD file, you can run `makepkg -i` in the same directory as the PKGBUILD file. Otherwise, you can install the package file using `pacman -U [PACKAGE_NAME].pkg.tar.xz` as superuser or with sudo—replacing `[PACKAGE_NAME]` with the name of your package file.

Note: if building using the PKGBUILD file, you can simply run `makepkg -si` or `makepkg -fi` to build the package and install it all in one line.

## Removal
Run `pacman -Rns pamac-flatpak-mobile` as superuser or with sudo. This will remove any unneeded dependencies as well.

## Running
If your desktop environment has an app drawer or launcher, you can open Pamac using the `Add/Remove Software` launcher icon. You can also start it from the command line using `pamac-manager`.

## FAQ
### Q: Why aren't apps populating the browse section?
A: This sometimes happens on Arch Linux due to an issue (in [appstream-glib](https://github.com/hughsie/appstream-glib/issues/350)) parsing community Appstream data ([source](https://gitlab.manjaro.org/applications/pamac/-/issues/772)). A fix for this (found on the AUR page for [pamac-aur](https://aur.archlinux.org/packages/pamac-aur/)) is to first run `zcat /usr/share/app-info/xmls/community.xml.gz | sed 's|<em>||g;s|<\/em>||g;' | gzip > "new.xml.gz"`. This will generate a `new.xml.gz` file in your working directory (remember to delete it once you're done). Then, run `cp new.xml.gz /usr/share/app-info/xmls/community.xml.gz` as superuser or with sudo to apply this newly created file. Finally, run `appstreamcli refresh-cache --force` as superuser or with sudo to refresh the cache that Appstream uses. Make sure you have `appstream` installed (`pacman -S appstream` as superuser or with sudo). You should now be able to see applications populate the browse section in Pamac. It is now safe to delete the `new.xml.gz` file we created earlier.

## Screenshots
### Browse
![screenshot_browse_0.png](.screenshots/screenshot_browse_0.png)
### Updates
![screenshot_updates_0.png](.screenshots/screenshot_updates_0.png)
